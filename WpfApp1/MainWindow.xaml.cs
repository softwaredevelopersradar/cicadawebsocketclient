﻿using CicadaWebSocketClientProject;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        CicadaWebSocketClient cicadaWebSocketClient = new CicadaWebSocketClient();

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            cicadaWebSocketClient.OnConnected += CicadaWebSocketClient_OnConnected;
            await cicadaWebSocketClient.IQTestConnectToWebServer();
        }

        private async void Button_ClickCicada(object sender, RoutedEventArgs e)
        {
            cicadaWebSocketClient.OnConnected += CicadaWebSocketClient_OnConnected;
            //await cicadaWebSocketClient.IQConnectToWebServer();
           var result =  await cicadaWebSocketClient.IQConnectToWebServer("192.168.0.75");
        }

        private void Button_Click_Disconnect(object sender, RoutedEventArgs e)
        {
            cicadaWebSocketClient.DisconnectFromWebServer();
        }

        private void CicadaWebSocketClient_OnConnected(bool onConnected)
        {
            Console.WriteLine(onConnected);

            if (onConnected)
            {
                cicadaWebSocketClient.BearingWSEvent += CicadaWebSocketClient_BearingWSEvent;
                cicadaWebSocketClient.SourcelocationWSEvent += CicadaWebSocketClient_SourcelocationWSEvent;
                cicadaWebSocketClient.SoundWSEvent += CicadaWebSocketClient_SoundWSEvent;

                cicadaWebSocketClient.PelengEvent += CicadaWebSocketClient_PelengEvent;
                cicadaWebSocketClient.ZvukEvent += CicadaWebSocketClient_ZvukEvent;
                cicadaWebSocketClient.MestoEvent += CicadaWebSocketClient_MestoEvent;

            }
            else
            {
                cicadaWebSocketClient.OnConnected -= CicadaWebSocketClient_OnConnected;

                cicadaWebSocketClient.BearingWSEvent -= CicadaWebSocketClient_BearingWSEvent;
                cicadaWebSocketClient.SourcelocationWSEvent -= CicadaWebSocketClient_SourcelocationWSEvent;
                cicadaWebSocketClient.SoundWSEvent -= CicadaWebSocketClient_SoundWSEvent;

                cicadaWebSocketClient.PelengEvent -= CicadaWebSocketClient_PelengEvent;
                cicadaWebSocketClient.ZvukEvent -= CicadaWebSocketClient_ZvukEvent;
                cicadaWebSocketClient.MestoEvent -= CicadaWebSocketClient_MestoEvent;
            }
        }


        private void CicadaWebSocketClient_PelengEvent(Peleng.Root root)
        {
            Console.WriteLine(root);
        }

        private void CicadaWebSocketClient_ZvukEvent(Zvuk.Root root)
        {
            Console.WriteLine(root);
        }

        private void CicadaWebSocketClient_MestoEvent(Mesto.Root root)
        {
            Console.WriteLine(root);
        }

        private void CicadaWebSocketClient_BearingWSEvent(BearingWS bearingWS)
        {
            Console.WriteLine(bearingWS);
        }

        private void CicadaWebSocketClient_SourcelocationWSEvent(SourcelocationWS sourcelocationWS)
        {
            Console.WriteLine(sourcelocationWS);
        }

        private void CicadaWebSocketClient_SoundWSEvent(SoundWS soundWS)
        {
            Console.WriteLine(soundWS);
        }

        private async void Button_Click_Code1(object sender, RoutedEventArgs e)
        {
            var result = await cicadaWebSocketClient.SendDefaultMessage();
        }

        private async void Button_Click_Code2(object sender, RoutedEventArgs e)
        {
            var result = await cicadaWebSocketClient.SendTestMessage(1);
        }

        private async void Button_Click_Code3(object sender, RoutedEventArgs e)
        {
            byte[] vs = Enumerable.Range(0, 1000).Select(x => (byte)x).ToArray();
            var result = await cicadaWebSocketClient.SendTestLongMessage(vs);
        }

        private async void Button_Click_BWS(object sender, RoutedEventArgs e)
        {
            BearingWS bearingWS = DirectFromJSON<BearingWS>();

            string jsonString = JsonConvert.SerializeObject(bearingWS);

            await cicadaWebSocketClient.SendJSONString(jsonString);
        }

        private async void Button_Click_SlWS(object sender, RoutedEventArgs e)
        {
            SourcelocationWS sourcelocationWS = DirectFromJSON<SourcelocationWS>();

            string jsonString = JsonConvert.SerializeObject(sourcelocationWS);

            await cicadaWebSocketClient.SendJSONString(jsonString);
        }

        private async void Button_Click_SWS(object sender, RoutedEventArgs e)
        {
            SoundWS soundWS = DirectFromJSON<SoundWS>();

            string jsonString = JsonConvert.SerializeObject(soundWS);

            await cicadaWebSocketClient.SendJSONString(jsonString);
        }

        private T DirectFromJSON<T>()
        {
            JObject jObject = JObject.Parse(File.ReadAllText($@"C:\Users\User\Downloads\Цикада-12\{typeof(T).Name}.json"));

            return jObject.ToObject<T>();
        }


    }
}
