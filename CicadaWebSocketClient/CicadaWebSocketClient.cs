﻿using CicadaProtocol;
using llcss;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CicadaWebSocketClientProject
{
    public class CicadaWebSocketClient
    {
        private ClientWebSocket clientWebSocket;


        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        private bool _IsConnected = false;
        public bool IsConnected
        {
            get { return _IsConnected; }
            private set
            {
                _IsConnected = value;
                OnConnected?.Invoke(value);
            }
        }

        private void IsConnectedChange(bool value)
        {
            IsConnected = value;
        }

        #region Events

        public delegate void OnConnectedEventHandler(bool onConnected);
        public event OnConnectedEventHandler OnConnected;

        public delegate void OnReadEventHandler(bool onRead);
        public event OnReadEventHandler OnRead;

        public delegate void OnWriteEventHandler(bool onWrite);
        public event OnWriteEventHandler OnWrite;

        #region OldEvents
        public delegate void BearingWSEventHandler(BearingWS bearingWS);
        public event BearingWSEventHandler BearingWSEvent;

        public delegate void SourcelocationWSEventHandler(SourcelocationWS sourcelocationWS);
        public event SourcelocationWSEventHandler SourcelocationWSEvent;

        public delegate void SoundWSEventHandler(SoundWS soundWS);
        public event SoundWSEventHandler SoundWSEvent;
        #endregion

        public delegate void PelengEventHandler(Peleng.Root root);
        public event PelengEventHandler PelengEvent;

        public delegate void ZvukEventHandler(Zvuk.Root root);
        public event ZvukEventHandler ZvukEvent;

        public delegate void MestoEventHandler(Mesto.Root root);
        public event MestoEventHandler MestoEvent;

        #endregion

        public CicadaWebSocketClient()
        {
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        static byte OwnServerAddress = 0;
        private byte ReceiverAddress = 1;
        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
        }

        private async Task ConnectToWebServer(string uriString, CancellationToken token)
        {
            clientWebSocket = new ClientWebSocket();

            while (clientWebSocket.State != WebSocketState.Open)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await clientWebSocket.ConnectAsync(new Uri(uriString), token);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    IsConnectedChange(false);
                    clientWebSocket = new ClientWebSocket();
                }
                //if (clientWebSocket.State != WebSocketState.Open)
                await Task.Delay(500);
            }

            await SendConnectJSON(new ConnectCicadaMessageMessage());

            Console.WriteLine("ConnectWebSocket");

            Task.Run(() => ReadThread2());

            IsConnectedChange(true);
        }

        public async Task<string> IQTestConnectToWebServer(string uriString = "wss://echo.websocket.org", int timeout = 30000)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;

            var task = ConnectToWebServer(uriString, token);

            if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
            {
                return "OK";
            }
            else
            {
                cts.Cancel();
                return "Invalid IP address or Server not found";
            }
        }

        public async Task<string> IQConnectToWebServer(string hostname = "127.0.0.1", int port = 18081, int timeout = 30000)
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(hostname, 0))
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;

                string uriString = $"ws://{hostname}:{port}";

                var task = ConnectToWebServer(uriString, token);

                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    //Console.WriteLine("task completed within timeout");
                    return "OK";
                }
                else
                {
                    cts.Cancel();
                    //Console.WriteLine("timeout logic");
                    return "Invalid IP address or Server not found";
                }
            }
            else
            {
                return "Invalid IP address";
            }
        }

        public void DisconnectFromWebServer()
        {
            clientWebSocket.Abort();
            clientWebSocket.Dispose();
            IsConnectedChange(false);
        }

        private async Task ReadThread2()
        {
            while (IsConnected)
            {
                List<byte> headerBuffer = new List<byte>();
                var count = 0;

                int BufferCount = 10240;

                try
                {
                    do
                    {
                        ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[BufferCount]);
                        WebSocketReceiveResult result = await clientWebSocket.ReceiveAsync(bytesReceived, CancellationToken.None);
                        count = result.Count;
                        headerBuffer.AddRange(bytesReceived.Array.Take(count));
                        OnRead?.Invoke(true);
                        await Task.Delay(1);
                    }
                    //while (count == BufferCount);
                    while (headerBuffer[headerBuffer.Count - 1] != 125 && headerBuffer[headerBuffer.Count - 2] != 125);
                }
                catch (Exception)
                {
                    OnRead?.Invoke(false);
                    IsConnectedChange(false);
                }
                if (count == 0)
                {
                    IsConnectedChange(false);
                    OnRead?.Invoke(false);
                    return;
                }

                string jsonString = Encoding.UTF8.GetString(headerBuffer.ToArray());

                //TryConnectCicadaMessageMessage(jsonString);

                //TryBearingWS(jsonString);
                //TryBearingWS2(jsonString);
                //TrySourcelocationWS(jsonString);
                //TrySoundWS(jsonString);

                TryRoot(jsonString);
            }
        }

        #region Old

        private void TryConnectCicadaMessageMessage(string jsonString)
        {
            try
            {
                ConnectCicadaMessageMessage connectCicadaMessage = JsonConvert.DeserializeObject<ConnectCicadaMessageMessage>(jsonString);
            }
            catch { }
        }

        private void TryBearingWS(string jsonString)
        {
            try
            {
                BearingWS bearingWS = JsonConvert.DeserializeObject<BearingWS>(jsonString);
                if (bearingWS.data != null)
                {
                    if (bearingWS.data.Count > 0)
                    {
                        if (bearingWS.data[0].nav != null)
                            BearingWSEvent?.Invoke(bearingWS);
                    }
                }
            }
            catch (Exception) { }
        }
        private void TryBearingWS2(string jsonString)
        {
            try
            {
                object obj = JsonConvert.DeserializeObject(jsonString);

                var resOrNull = obj as BearingWS;

                var resOrNull2 = (BearingWS)obj;

                JObject o = JObject.Parse(jsonString);
            }
            catch (Exception) { }
        }
        private void TrySourcelocationWS(string jsonString)
        {
            try
            {
                SourcelocationWS sourcelocationWS = JsonConvert.DeserializeObject<SourcelocationWS>(jsonString);
                if (sourcelocationWS.data != null)
                {
                    if (sourcelocationWS.data.Count > 0)
                    {
                        if (sourcelocationWS.data[0].description != null)
                        {
                            SourcelocationWSEvent?.Invoke(sourcelocationWS);
                        }
                    }
                }
            }
            catch (Exception) { }
        }
        private void TrySoundWS(string jsonString)
        {
            try
            {
                SoundWS soundWS = JsonConvert.DeserializeObject<SoundWS>(jsonString);
                if (soundWS.data != null)
                {
                    SoundWSEvent?.Invoke(soundWS);
                }
            }
            catch { }
        }

        #endregion

        //Main F
        private void TryRoot(string jsonString)
        {
            JObject jObject = JObject.Parse(jsonString);

            string subType = "";

            if (jObject.ContainsKey("ws"))
            {
                var ws = jObject["ws"];

                subType = ws["subType"].ToString();
            }

            switch (subType)
            {

                case "BearingResultType":
                    try
                    {
                        Peleng.Root myDeserializedClass1 = JsonConvert.DeserializeObject<Peleng.Root>(jsonString);
                        if (myDeserializedClass1 != null)
                            if (myDeserializedClass1.data != null)
                            {
                                if (myDeserializedClass1.data.Count > 0)
                                {
                                    if (myDeserializedClass1.data[0].nav != null)
                                    {
                                        PelengEvent?.Invoke(myDeserializedClass1);
                                        return;
                                    }
                                }
                            }
                    }
                    catch (Exception e) { Console.WriteLine(e); }
                    break;

                case "SourceLocationResultType":
                    try
                    {
                        Mesto.Root myDeserializedClass3 = JsonConvert.DeserializeObject<Mesto.Root>(jsonString);
                        if (myDeserializedClass3 != null)
                            if (myDeserializedClass3.data != null)
                            {
                                MestoEvent?.Invoke(myDeserializedClass3);
                                return;
                            }
                    }
                    catch { }
                    break;

                case "SoundType":
                    try
                    {
                        Zvuk.Root myDeserializedClass2 = JsonConvert.DeserializeObject<Zvuk.Root>(jsonString);
                        if (myDeserializedClass2 != null)
                            if (myDeserializedClass2.data != null)
                            {
                                ZvukEvent?.Invoke(myDeserializedClass2);
                                return;
                            }
                    }
                    catch { }
                    break;

                default:
                    break;
            }
        }

        private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message, WebSocketMessageType webSocketMessageType = WebSocketMessageType.Binary)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                var receiveTask = new TaskCompletionSource<IBinarySerializable>();
                if (!concurrentDictionary.ContainsKey(header.Code))
                {
                    concurrentDictionary[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
                }
                concurrentDictionary[header.Code].Enqueue(receiveTask);

                int count = message.Count();
                try
                {
                    ArraySegment<byte> arraySegment = new ArraySegment<byte>(message);
                    await clientWebSocket.SendAsync(arraySegment, WebSocketMessageType.Binary, true, CancellationToken.None);
                    OnWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    OnWrite?.Invoke(false);
                    receiveTask.SetException(new Exception("No connection"));
                }

                return receiveTask;
            }
        }

        //Шифр 1 
        public async Task<DefaultMessage> SendDefaultMessage()
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<TestMessage> SendTestMessage(byte data)
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = TestMessage.ToBinary(header, data);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                TestMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as TestMessage;

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                TestMessage answer = new TestMessage(header, 0);
                return answer;
            }
        }

        //Шифр 3
        public async Task<TestLongMessage> SendTestLongMessage(byte[] data)
        {
            if (IsConnected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: TestLongMessage.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = TestLongMessage.ToBinary(header, data);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                TestLongMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as TestLongMessage;

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                TestLongMessage answer = new TestLongMessage(header, new byte[] { });
                return answer;
            }
        }

        public async Task SendConnectJSON(ConnectCicadaMessageMessage connectCicadaMessage, WebSocketMessageType webSocketMessageType = WebSocketMessageType.Text)
        {
            string jsonString = JsonConvert.SerializeObject(connectCicadaMessage);

            byte[] bytes = Encoding.UTF8.GetBytes(jsonString);

            ArraySegment<byte> arraySegment = new ArraySegment<byte>(bytes);

            await clientWebSocket.SendAsync(arraySegment, webSocketMessageType, true, CancellationToken.None);

        }

        public async Task SendJSONString(string jsonString, WebSocketMessageType webSocketMessageType = WebSocketMessageType.Binary)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(jsonString);

            ArraySegment<byte> arraySegment = new ArraySegment<byte>(bytes);

            await clientWebSocket.SendAsync(arraySegment, webSocketMessageType, true, CancellationToken.None);
        }
    }
}
