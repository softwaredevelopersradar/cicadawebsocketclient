﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CicadaWebSocketClientProject
{
    public class ConnectCicadaMessageMessage
    {
        public string command { get; set; } = "subscribe";
        public Message message { get; set; }
        public string path { get; set; } = "/";
        public SubsciptionLevel subsciptionLevel { get; set; }

        public ConnectCicadaMessageMessage()
        {
            message = new Message();
            subsciptionLevel = new SubsciptionLevel();
        }
    }

    public class Message
    {
        public string servicepath { get; set; } = "/AW/1.0.0/cikada-sou";
    }

    public class SubsciptionLevel
    {
        public int level { get; set; } = 0;
    }

   
}
