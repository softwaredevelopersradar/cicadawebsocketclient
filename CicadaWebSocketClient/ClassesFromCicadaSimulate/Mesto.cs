﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesto
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Ws
    {
        public string abonent_guid { get; set; }
        public int band { get; set; }
        public string description { get; set; }
        public DateTime endtime { get; set; }
        public int frequency { get; set; }
        public int group { get; set; }
        public string id { get; set; }
        public int id_inc { get; set; }
        public List<int> id_post { get; set; }
        public List<string> id_post_names { get; set; }
        public bool isValid { get; set; }
        public bool is_closed { get; set; }
        public object jsondata { get; set; }
        public bool leftCut { get; set; }
        public bool processed { get; set; }
        public object realendtime { get; set; }
        public object realstarttime { get; set; }
        public bool rightCut { get; set; }
        public int slot { get; set; }
        public string source { get; set; }
        public string source_fk { get; set; }
        public DateTime starttime { get; set; }
        public string subType { get; set; }
        public string target_abonent_guid { get; set; }
        public int user_data { get; set; }
        public bool visible { get; set; }
        public int wstype { get; set; }
    }

    public class Root
    {
        public List<Datum> data { get; set; }
        public string path { get; set; }
        public string state { get; set; }
        public Ws ws { get; set; }
    }

    public class Datum
    {
        public int altitude { get; set; }
        public DateTime datetime { get; set; }
        public string description { get; set; }
        public int ellipseAlpha { get; set; }
        public int ellipseBetta { get; set; }
        public int ellipseGamma { get; set; }
        public double latitude { get; set; }
        public double level { get; set; }
        public double longitude { get; set; }
        public int pitch { get; set; }
        public double quality { get; set; }
        public int roll { get; set; }
        public int uncertainty { get; set; }
        public int velocity { get; set; }
        public int yaw { get; set; }
    }
}
