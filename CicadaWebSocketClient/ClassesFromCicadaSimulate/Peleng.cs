﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peleng
{
    
    public class Nav
    {
        public int altitude { get; set; }
        public int bearing { get; set; }
        public int chipFlags { get; set; }
        public int debug { get; set; }
        public int doubleTimeStamp { get; set; }
        public int filter_bandwidth { get; set; }
        public int filter_flags { get; set; }
        public int filter_freq { get; set; }
        public int fpgaTicker { get; set; }
        public int fpgaTicker2 { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int pitch { get; set; }
        public int precisionPos { get; set; }
        public int precisionVel { get; set; }
        public double quality { get; set; }
        public int roll { get; set; }
        public int sendFlags { get; set; }
        public int svCnt { get; set; }
        public int svCntRcv { get; set; }
        public object timeStamp { get; set; }
        public int timeStampNew { get; set; }
        public DateTime timeUTC { get; set; }
        public object timeUTCEnd { get; set; }
        public int tow { get; set; }
        public int velocity { get; set; }
        public int vx { get; set; }
        public int vy { get; set; }
        public int vz { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int yaw { get; set; }
        public int z { get; set; }
    }

    public class Datum
    {
        public double azimut { get; set; }
        public DateTime date_time { get; set; }
        public double level { get; set; }
        public Nav nav { get; set; }
        public double quality { get; set; }
    }

    public class Jsondata
    {
        public double BearingSKO { get; set; }
        public double MeanBearing { get; set; }
    }

    public class Ws
    {
        public string abonent_guid { get; set; }
        public int band { get; set; }
        public string description { get; set; }
        public DateTime endtime { get; set; }
        public int frequency { get; set; }
        public int group { get; set; }
        public string id { get; set; }
        public int id_inc { get; set; }
        public List<int> id_post { get; set; }
        public List<string> id_post_names { get; set; }
        public bool isValid { get; set; }
        public bool is_closed { get; set; }
        public Jsondata jsondata { get; set; }
        public bool leftCut { get; set; }
        public bool processed { get; set; }
        public object realendtime { get; set; }
        public object realstarttime { get; set; }
        public bool rightCut { get; set; }
        public int slot { get; set; }
        public string source { get; set; }
        public string source_fk { get; set; }
        public DateTime starttime { get; set; }
        public string subType { get; set; }
        public string target_abonent_guid { get; set; }
        public int user_data { get; set; }
        public bool visible { get; set; }
        public int wstype { get; set; }
    }

    public class Root
    {
        public List<Datum> data { get; set; }
        public string path { get; set; }
        public string state { get; set; }
        public Ws ws { get; set; }
    }


}
