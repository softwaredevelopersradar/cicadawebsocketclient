﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CicadaWebSocketClientProject
{
    public class SourcelocationWS
    {
        public List<Sourcelocation> data { get; set; }
        public string path { get; set; }
        public string state { get; set; }
        public Ws ws { get; set; }
    }

    public class Sourcelocation
    {
        public int altitude { get; set; }
        public DateTime datetime { get; set; }
        public string description { get; set; }
        public int ellipseAlpha { get; set; }
        public int ellipseBetta { get; set; }
        public int ellipseGamma { get; set; }
        public double latitude { get; set; }
        public double level { get; set; }
        public double longitude { get; set; }
        public int pitch { get; set; }
        public double quality { get; set; }
        public int roll { get; set; }
        public int uncertainty { get; set; }
        public int velocity { get; set; }
        public int yaw { get; set; }
    }
}
