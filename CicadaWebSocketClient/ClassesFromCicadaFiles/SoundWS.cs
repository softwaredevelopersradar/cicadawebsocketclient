﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CicadaWebSocketClientProject
{
    public class SoundWS
    {
        public string data { get; set; }
        public DataInfo dataInfo { get; set; }
        public string path { get; set; } = "/AW/1.0.0/cikada-sou";
        public string state { get; set; }
        public Ws ws { get; set; }
    }

    public class DataInfo
    {
        public string demodulation { get; set; }
        public int sampleFrequency { get; set; }
        public string type { get; set; }
    }

    public class Ws
    {
        public string abonent_guid { get; set; }
        public int band { get; set; }
        public string description { get; set; }
        public DateTime endtime { get; set; }
        public int frequency { get; set; }
        public int group { get; set; }
        public string id { get; set; }
        public int id_inc { get; set; }
        public List<int> id_post { get; set; }
        public List<string> id_post_names { get; set; }
        public bool isValid { get; set; }
        public bool is_closed { get; set; }
        public Jsondata jsondata { get; set; }
        public bool leftCut { get; set; }
        public bool processed { get; set; }
        public DateTime realendtime { get; set; }
        public DateTime realstarttime { get; set; }
        public bool rightCut { get; set; }
        public int slot { get; set; }
        public string source { get; set; }
        public string source_fk { get; set; }
        public DateTime starttime { get; set; }
        public string subType { get; set; }
        public string target_abonent_guid { get; set; }
        public string user_data { get; set; }
        public bool visible { get; set; }
        public int wstype { get; set; }
    }

    public class Jsondata
    {
        public int colorCode { get; set; }
        public string comment { get; set; }
        public int demodulationFk { get; set; }
        public string dmrCallType { get; set; }
        public string duplexUUID { get; set; }
        public int duration { get; set; }
        public int reservedFlag { get; set; }
        public int sampleFreq { get; set; }
        public int typeFK { get; set; }
    }
}
