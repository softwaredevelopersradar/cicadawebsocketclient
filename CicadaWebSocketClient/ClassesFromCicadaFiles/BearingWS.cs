﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CicadaWebSocketClientProject
{
    public class BearingWS
    {
        public List<Bearing> data { get; set; }
        public string path { get; set; }
        public string state { get; set; }
        public Ws ws { get; set; }
    }

    public class Bearing
    {
        public double azimut { get; set; }
        public DateTime date_time { get; set; }
        public double level { get; set; }
        public Nav nav { get; set; }
        public double quality { get; set; }
    }

    public class Nav
    {
        public int altitude { get; set; }
        public int bearing { get; set; }
        public int chipFlags { get; set; }
        public int debug { get; set; }
        public int doubleTimeStamp { get; set; }
        public int filter_bandwidth { get; set; }
        public int filter_flags { get; set; }
        public int filter_freq { get; set; }
        public int fpgaTicker { get; set; }
        public int fpgaTicker2 { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int pitch { get; set; }
        public int precisionPos { get; set; }
        public int precisionVel { get; set; }
        public int quality { get; set; }
        public int roll { get; set; }
        public int sendFlags { get; set; }
        public int svCnt { get; set; }
        public int svCntRcv { get; set; }
        public object timeStamp { get; set; }
        public int timeStampNew { get; set; }
        public DateTime timeUTC { get; set; }
        public object timeUTCEnd { get; set; }
        public int tow { get; set; }
        public int velocity { get; set; }
        public int vx { get; set; }
        public int vy { get; set; }
        public int vz { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int yaw { get; set; }
        public int z { get; set; }
    }
}
